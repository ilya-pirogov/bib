import pickle
from xml.dom.minidom import parseString
from os.path import dirname, join, exists
import re
from irc3.plugins.command import command
import irc3
import requests


YOUTUBE_FEED_URL = 'http://gdata.youtube.com/feeds/api/videos/{}'
NOT_FOUND_ERROR = 'Song was not found'
BID_ERROR = 'Incorrect bid'
WELCOME_MSG = 'Welcome, {}! Use command: "!sign <youtube_url_or_id> <bid>" ' \
              'to do song request. Also you can follow the current songs ' \
              'queue on the site: http://bib.pirogov.me/'
QUEUE_FILE = join(dirname(__file__), 'queue')

songs = []


@irc3.plugin
class BibPlugin:
    """A plugin is a class which take the IrcBot as argument
    """

    requires = [
        'irc3.plugins.core',
        'irc3.plugins.userlist',
        'irc3.plugins.command',
        'irc3.plugins.human',
    ]

    def __init__(self, bot):
        self.bot = bot
        self.log = self.bot.log

    def connection_made(self):
        if exists(QUEUE_FILE):
            with open(QUEUE_FILE, 'rb') as fp:
                songs.clear()
                songs.extend(pickle.load(fp))

    def server_ready(self):
        """triggered after the server sent the MOTD (require core plugin)"""

    def connection_lost(self):
        with open(QUEUE_FILE, 'wb') as fp:
            pickle.dump(songs, fp)

    @irc3.event(irc3.rfc.JOIN)
    def welcome(self, mask, channel):
        print(mask, channel)
        """Welcome people who join a channel"""
        if mask.nick != self.bot.nick:
            self.bot.call_with_human_delay(
                self.bot.privmsg, channel, WELCOME_MSG.format(mask.nick))

    @command()
    def sing(self, mask, target, args):
        """Request a song to MJRamon

            %%sing <youtube_id> <bid>
        """
        try:
            bid = int(args['<bid>'])
        except ValueError:
            self.bot.privmsg(target, BID_ERROR)
            return

        if bid < 1:
            self.bot.privmsg(target, BID_ERROR)
            return

        yid = args['<youtube_id>']
        if yid.startswith('https://'):
            m = re.search('watch\?v=([\w\d_\-]+)', yid)
            if not m:
                self.bot.privmsg(target, NOT_FOUND_ERROR)
                return
            yid = m.group(1)
        title = self.get_youtube_title(yid)

        if not title:
            self.bot.privmsg(target, NOT_FOUND_ERROR)
        else:
            self.request_song(target, mask.nick, yid, bid, title)

    @irc3.extend
    def request_song(self, target, nick, yid, bid, title):
        url = 'https://www.youtube.com/watch?v={}'.format(yid)
        self.bot.privmsg(target, 'Song: {} ({})'.format(title, url))
        self.bot.privmsg(target, '!remove {} {}'.format(bid, nick))
        self.bot.privmsg(target, '!requestsong {}'.format(yid))
        songs.append(
            (nick, title, bid, url, yid)
        )

    @irc3.extend
    def get_youtube_title(self, yid):
        url = YOUTUBE_FEED_URL.format(yid)
        response = requests.get(url)
        xml = parseString(response.content)
        title = xml.getElementsByTagName('title')
        if not title:
            return None
        return title[0].firstChild.nodeValue