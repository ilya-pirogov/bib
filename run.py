#!/usr/bin/env python3
import signal
import bib
import irc3
import os
from bottle import view
from aiobottle import AsyncBottle


bot = irc3.IrcBot(
    nick=os.environ['BIB_NICK'], autojoins=[os.environ['BIB_CHANNEL']],
    debug=True,
    host='irc.twitch.tv', port=6667, ssl=False,
    password=os.environ['BIB_PASSWORD'],
    includes=[
        'irc3.plugins.core',
        'irc3.plugins.command',
        'irc3.plugins.human',
        'irc3.plugins.log',
        'bib',
    ])
loop = bot.create_connection()
loop.add_signal_handler(signal.SIGHUP, bot.SIGHUP)
loop.add_signal_handler(signal.SIGINT, bot.SIGINT)

app = AsyncBottle()


@app.route('/')
@view('index')
def index():
    return {
        'songs': [[n+1] + list(i) for n, i in
                  enumerate(sorted(bib.songs, key=lambda x: x[2], reverse=True))]
    }


def main(host='localhost', port=8080):
    app.run(host=host, port=port, server='aiobottle:AsyncServer')

if __name__ == '__main__':
    main()