
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Karaoke with MJRamon</title>

    <!-- Bootstrap core CSS -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">
  </head>

  <body>

    <nav class="navbar navbar-inverse">
      <div class="container">
          <a class="navbar-brand" href="/">Karaoke with MJRamon</a>
      </div>
    </nav>

    <div class="container">

        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nick</th>
                    <th>Song</th>
                    <th>Bid</th>
                    <th>YID</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                % for song in songs:
                <tr>
                    <td>{{ song[0] }}</td>
                    <td><a href="http://www.twitch.tv/{{ song[1] }}">{{ song[1] }}</a></td>
                    <td><a href="{{ song[4] }}">{{ song[2] }}</a></td>
                    <td>{{ song[3] }}</td>
                    <td>{{ song[5] }}</td>
                    <td><button class="btn btn-danger btn-xs">x</button></td>
                </tr>
                % end
            </tbody>
        </table>

    </div>

    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
  </body>
</html>
